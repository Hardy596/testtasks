﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestTask.Models;

namespace TestTask.Controllers
{
    public class HomeController : Controller
    {
        Dictionary<string, Currency> currencies;
        Daily daily;
        int pageSize = 5;
        int totalItems;
        int totalPages;

        public HomeController()
        {
            var request = (HttpWebRequest)WebRequest.Create("https://www.cbr-xml-daily.ru/daily_json.js");
            request.Method = "GET";
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            daily = JsonConvert.DeserializeObject<Daily>(responseString);
            currencies = daily.Valute;
            totalItems = currencies.Count();
            totalPages = (int)Math.Ceiling((decimal)totalItems / pageSize);
        }

        public ActionResult Index(int page = 1)
        {
            var curPages = currencies.Values.Skip((page - 1) * pageSize).Take(pageSize);
            return View(new ModelCurrency {
                PageNumber = page,
                PageSize = pageSize,
                TotalItems = totalItems,
                TotalPages = totalPages,
                Currencies = curPages
            });
        }

        [HttpGet]
        public ActionResult Currency(string id)
        {
            var q = currencies.Values.Where(cur => cur.ID == id).FirstOrDefault();
            ViewBag.SelCur = q;
            return View();
        }
    }
}