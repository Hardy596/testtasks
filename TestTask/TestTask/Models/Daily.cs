﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTask.Models
{
    public class Daily
    {
        public DateTime Date { get; set; }
        public DateTime PreviousDate { get; set; }
        public String PreviousURL { get; set; }
        public DateTime Timestamp { get; set; }
        public Dictionary<String, Currency> Valute { get; set; }
    }
}